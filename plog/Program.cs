﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using System.Collections.Generic;
using ploggy;

namespace plog
{
	struct UnitTypeHighlightItem
	{
		public UnitType	unitType;
		public string 	format;

		public UnitTypeHighlightItem(UnitType unitType, string format)
		{
			this.unitType = unitType;
			this.format = format;
		}
	}

    struct KillDeathHighlightItem
	{
		public KillDeathType	deathType;
		public string 	        format;

        public KillDeathHighlightItem(KillDeathType deathType, string format)
		{
            this.deathType = deathType;
			this.format = format;
		}
	}

	struct BriefStat
	{
		public MissionStats 			MissionStats;
		public List<DeathSourceClassifier> DeadStats;
		public List<PlayerStatsInfo>	PlayerStats;
	}

	class MainClass
	{
        private static GameObjectsData LoadGameData()
		{
            Dictionary<string, UnitType> gameData = null;

			JsonSerializer serializer = new JsonSerializer();
			serializer.Converters.Add(new StringEnumConverter());

			using (StreamReader sw = new StreamReader("GameObjects.json"))
			using (JsonReader reader = new JsonTextReader(sw))
			{
				gameData = serializer.Deserialize<Dictionary<string, UnitType> >(reader);
			}

            return new GameObjectsData(gameData);
		}

		private static UnitTypeHighlightItem[] playerUnitFields = 
        {
            new UnitTypeHighlightItem(UnitType.Fighter, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.Attacker, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.Bomber, "{0,4}"),

			new UnitTypeHighlightItem(UnitType.AAA, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.FiringPoint, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.Artillery, "{0,4}"),

			new UnitTypeHighlightItem(UnitType.Tank, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.Vehicle, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.Turret, "{0,4}"),

			new UnitTypeHighlightItem(UnitType.Bot, "{0,4}"),
			new UnitTypeHighlightItem(UnitType.Static, "{0,5}  ")
		};

        private static KillDeathHighlightItem[] playerKillFields = {
			new KillDeathHighlightItem(KillDeathType.PilotKill, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.GunnerKill, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.VehicleKill, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.OtherKill, "{0,3}  "),
            
            new KillDeathHighlightItem(KillDeathType.FlightCount, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.Crash, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.Landed, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.Eject, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.LeftMission, "{0,3}  "),
            new KillDeathHighlightItem(KillDeathType.Death, "{0,3}"),
            new KillDeathHighlightItem(KillDeathType.VehicleLost, "{0,3}  "),
		};

		private static void SerializeToJson(Object obj, string filename)
		{
			JsonSerializer serializer = new JsonSerializer();
			serializer.Converters.Add(new StringEnumConverter());
			serializer.NullValueHandling = NullValueHandling.Include;
			serializer.Formatting = Formatting.Indented;

			using (StreamWriter sw = new StreamWriter(filename))
			using (JsonWriter writer = new JsonTextWriter(sw))
			{
				serializer.Serialize(writer, obj);
			}
		}

        private static ConsoleColor CoalitionColor(Coalition coalition, bool highlight = false)
        {
            switch (coalition)
            {
                case Coalition.Neutral:
                    return highlight ? ConsoleColor.DarkGray : ConsoleColor.Black;

                case Coalition.Allies:
                    return highlight ? ConsoleColor.Red : ConsoleColor.DarkRed;

                case Coalition.Axis:
                    return highlight ? ConsoleColor.Blue : ConsoleColor.DarkBlue;
            }

            return ConsoleColor.Black;
        }

        public static void DumpCountries(MissionLog missionLog)
        {
            CoalitionBind[] countries = missionLog.MissionCountries;

            if (countries == null)
                return;

            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("      Country      Total  PF  PA  PB AAA FPT ART TNK VEH TRT BOT STATIC UNKNOWN");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();

            for (int i = 0; i < countries.Length; ++i)
            {
                CoalitionBind bind = countries[i];

                if (bind.country == Country.None)
                    continue;

                if (bind.coalition == Coalition.All)
                    continue;

                System.Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = CoalitionColor(bind.coalition);
                
                int countryIndex = Utility.CountryIndex(missionLog.MissionCountries, bind.country);
                if (countryIndex == -1)
                    continue;

                CountryFrags frags = missionLog.MissionStats.FragPerCountry[countryIndex];
                //System.Console.WriteLine("    Country      Total  PF  PA  PB AAA FPT ART TNK VEH TRT BOT STATIC UNKNOWN");
                Console.Write("{0,13}{1,9}{2,5}{3,4}{4,4}{5,4}{6,4}{7,4}{8,4}{9,4}{10,4}{11,4}{12,7}{13,7}",
                    bind.country, frags.Frags,
                    frags.UnitTypes[(int)UnitType.Fighter].Count, frags.UnitTypes[(int)UnitType.Attacker].Count,frags.UnitTypes[(int)UnitType.Bomber].Count,
                    frags.UnitTypes[(int)UnitType.AAA].Count, frags.UnitTypes[(int)UnitType.FiringPoint].Count, frags.UnitTypes[(int)UnitType.Artillery].Count,
                    frags.UnitTypes[(int)UnitType.Tank].Count, frags.UnitTypes[(int)UnitType.Vehicle].Count,
                    frags.UnitTypes[(int)UnitType.Turret].Count, frags.UnitTypes[(int)UnitType.Bot].Count,
                    frags.UnitTypes[(int)UnitType.Static].Count, frags.UnitTypes[(int)UnitType.Unknown].Count);

                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public static void PrintPlayersHeader()
        {
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.ForegroundColor = ConsoleColor.White;
            // P G V O E
            Console.Write("                    Name         Country   K  G  V  O   FL CR LD EJ LM   DE VL  |  PF  PA  PB AAA FPT ART TNK VEH TRT BOT STATIC");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }

        public static void DumpPlayers(MissionLog missionLog)
        {

            CoalitionBind[] countries = missionLog.MissionCountries;

            if (countries == null)
                return;

            PrintPlayersHeader();
           
            Player[] players = missionLog.AllPlayers;

            if (players == null)
                return;

            int[] maxCoalitionKills = new int[(int)Coalition.All];
            int[] maxCoalitionKillsIndex = new int[(int)Coalition.All];
            PlayerStatsInfo[] psi = new PlayerStatsInfo[players.Length];

            for (int i = 0; i < maxCoalitionKillsIndex.Length; ++i)
                maxCoalitionKillsIndex[i] = -1;
             
            for (int i = 0; i < players.Length; ++i)
            {
                psi[i] = players[i].GetStats(missionLog.MissionCountries);
                int[] killDeathCounters = psi[i].GetKillDeathCounters();

                Country playerCountry = players[i].GetPriorityCountry(countries);
                // XXX: not safe
                int cIndex = (int)Utility.GetCountryCoalition(countries, playerCountry);
                int kills = killDeathCounters[(int)KillDeathType.PilotKill] +
                            killDeathCounters[(int)KillDeathType.GunnerKill] +
                            killDeathCounters[(int)KillDeathType.VehicleKill] +
                            killDeathCounters[(int)KillDeathType.OtherKill];

                if (maxCoalitionKills[cIndex] > kills)
                    continue;

                if (maxCoalitionKills[cIndex] < kills)
                {
                    maxCoalitionKills[cIndex] = kills;
                    maxCoalitionKillsIndex[cIndex] = i;
                    continue;
                }

                if (maxCoalitionKills[cIndex] == 0)
                    continue;

                int prevChampIndex = maxCoalitionKillsIndex[cIndex];

                if (psi[prevChampIndex].Deaths < psi[i].Deaths)
                    continue;

                maxCoalitionKillsIndex[cIndex] = i;
            }


            for (int i = 0; i < players.Length; ++i)
            {
                Country playerCountry = players[i].GetPriorityCountry(countries);
                Coalition coalition = Utility.GetCountryCoalition(countries, playerCountry);

                if (coalition == Coalition.All)
                    continue;

                System.Console.ForegroundColor = ConsoleColor.White;
                bool highlight = i == maxCoalitionKillsIndex[(int)coalition];

                Console.BackgroundColor = CoalitionColor(coalition, highlight);

                Console.Write("{0,24}{1,16}{2}",
                    players[i].Name, playerCountry, players[i].ChangedCountry ? "*" : " ");


                int[] killDeathCounters = psi[i].GetKillDeathCounters();

                for (int h = 0; h < playerKillFields.Length; ++h)
                {
                    int value = killDeathCounters[(int)playerKillFields[h].deathType];

                    if (value > 0)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        //Console.BackgroundColor = CoalitionHiglightedColor(coalition);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        //Console.BackgroundColor = CoalitionColor(coalition);
                    }

                    Console.Write(playerKillFields[h].format, value);
                }

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("|");

                for (int h = 0; h < playerUnitFields.Length; ++h)
                {
                    int value = psi[i].KilledUnitsCount[(int)playerUnitFields[h].unitType].Count;

                    if (value > 0)
                        Console.ForegroundColor = ConsoleColor.White;
                    else
                        Console.ForegroundColor = ConsoleColor.DarkGray;

                    Console.Write(playerUnitFields[h].format, value);
                }

                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }

            PrintPlayersHeader();
        }

        public static void DumpPlayer(MissionLog missionLog, string logId)
        {
            CoalitionBind[] countries = missionLog.MissionCountries;

            if (countries == null)
                return;

            Player player = missionLog.GetSinglePlayer();

            if (player == null)
                return;

            PlayerGameSession gameSession = player.GetCurrentGameSession();

            if (gameSession == null)
                return;

            Console.BackgroundColor = ConsoleColor.DarkYellow;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Pilot File {0}:\n\tName: {1}\tMission: {2}\tDate: {3}\n\tAircraft:{4}\tFlight Status: ", 
                logId, player.Name, missionLog.MissionType, missionLog.MissionTime,  gameSession.Vehicle);

            switch (gameSession.FinishState)
            {
                case SessionFinishState.Crashed:
                case SessionFinishState.Killed:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case SessionFinishState.Ejected:
                case SessionFinishState.LeftMission:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;

                case SessionFinishState.Landed:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }

            Console.WriteLine("{0}", gameSession.FinishState);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            Console.WriteLine("\n\nFlight Diary:");

            AmmunitionStatsEvent ammunitionEvent = null;
            float damage = 0.0f;
            MissionObject lastAttacker = null;
            TimeSpan lastReported = TimeSpan.Zero;
            int eventCount = 5;

            foreach (MissionEvent evt in gameSession.AllEvents)
            {
                // ignored events
                switch (evt.Event)
                {
                    case MissionEventType.MissionStart:
                    case MissionEventType.PlayerBegin:
                        continue;

                    case MissionEventType.AmmunitionStats:
                        ammunitionEvent = evt as AmmunitionStatsEvent;
                        continue;

                    case MissionEventType.Hit:
                        continue;

                    case MissionEventType.Damage:
                        {
                            DamageEvent damageEvent = evt as DamageEvent;

                            if (damageEvent.Damage > 0.5)
                                break;
                        }
                        continue;

                    case MissionEventType.GotDamage:
                        {
                            GotDamageEvent damageEvent = evt as GotDamageEvent;

                            damage += damageEvent.Damage;
                            lastAttacker = damageEvent.GetAttackerObject() ?? lastAttacker;
                        }
                        continue;

                    case MissionEventType.GotHit:
                        {
                            GotHitEvent hitEvent = evt as GotHitEvent;
                            lastAttacker = hitEvent.GetAttackerObject() ?? lastAttacker;
                        }
                        continue;

                    case MissionEventType.MissionEnd:
                    case MissionEventType.PlayerEnd:
                    case MissionEventType.RoundEnd:
                        continue;
                }

                TimeSpan duration = (evt.Timestamp - lastReported).Duration();
                int minutesPassed = duration.Minutes;
                ++eventCount;

                if ((minutesPassed > 10) || (eventCount >= 5) || 
                    evt.Event == MissionEventType.VehicleDestroyed || 
                    evt.Event == MissionEventType.BotDeath ||
                    evt.Event == MissionEventType.Kill)
                {
                    eventCount = 0;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("\n{0}", missionLog.MissionTime + evt.Timestamp);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine();
                    lastReported = evt.Timestamp;
                }

                switch (evt.Event)
                {
                    case MissionEventType.TakeOff:
                        Console.WriteLine("\tBegan mission. Take Off as scheduled.");

                        if (ammunitionEvent != null)
                        {
                            Console.WriteLine("\tAmmo supply: {0} bombs, {1} rockets, {2} shells, {3} bullets",
                                ammunitionEvent.Bombs, ammunitionEvent.Rockets, ammunitionEvent.Shells, ammunitionEvent.Bullets);
                            Console.WriteLine("\tFuel: {0}", ammunitionEvent.Fuel);
                        }
                        break;

                    case MissionEventType.Landing:
                        {
                            LandingEvent landing = evt as LandingEvent;
                            Console.WriteLine("\tSuccessfully landed at airbase in {0}.", Utility.GetMapSquareFromPosition(landing.Position));
                        }
                        break;

                    case MissionEventType.Damage:
                        {
                            DamageEvent damageEvent = evt as DamageEvent;
                            MissionObject target = damageEvent.GetTargetObject();

                            if (damageEvent.Damage > 0.5)
                            {
                                if (target.MissionObjectType == MissionObjectType.Pilot)
                                {
                                    MissionObject obj = missionLog.GetObjectEvenIfInactive(target.ParentId);
                                    Console.WriteLine("\tHeavily wounded {0} pilot of {1}",
                                    Utility.GetRelationship(countries, player.Country, target.Country), 
                                    obj != null ? obj.Type : "unidentified aircraft.");
                                }
                                else

                                Console.WriteLine("\tHeavily damaged {0} {1}",
                                    Utility.GetRelationship(countries, player.Country, target.Country), 
                                    target.Type);
                            }
                        }
                        break;

                    case MissionEventType.Kill:
                        {
                            KillEvent killEvent = evt as KillEvent;
                            MissionObject target = killEvent.GetTargetObject();
                            Relationship relationship = Utility.GetRelationship(countries, gameSession.Country, target.Country);
                            if (target.MissionObjectType == MissionObjectType.Pilot)
                            {
                                MissionObject parent = missionLog.GetObjectEvenIfInactive(target.ParentId);

                                if (parent != null)
                                    Console.Write("\tBrought down to earth {0} pilot of {1}", 
                                        relationship, parent.Type);
                            }
                            else if (target.UnitType == UnitType.Static)
                            {
                                Console.Write("\tDestroyed {0} object {1}",
                                        relationship, target.Name);

                            } else
                                Console.Write("\tDestroyed {0} {1} {2}",
                                    relationship, target.UnitType, target.Type);

                            Console.WriteLine(" at map square {0} ({1}; {2})", 
                                Utility.GetMapSquareFromPosition(target.Position), target.Position.X, target.Position.Z);
                        }
                        break;

                    case MissionEventType.Ejected:
                        {
                            EjectEvent eject = evt as EjectEvent;

                            if (damage > 0.7)
                                Console.WriteLine("\tGot heavy damage and under fire from {0}", lastAttacker);
                            Console.WriteLine("\tDecided to leave aircraft at map square {0}", Utility.GetMapSquareFromPosition(eject.Position));
                        }
                        break;

                    case MissionEventType.BotDeath:
                        {
                            BotDeathEvent deathEvent = evt as BotDeathEvent;
                            MissionObject target = deathEvent.GetTargetObject();
                            UnitType unitType = deathEvent.GetBotType();

                            if (unitType == UnitType.Vehicle)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("\tKILLED IN ACTION.");
                                Console.ForegroundColor = ConsoleColor.Gray;
                                Console.WriteLine("\tReported death place: {0}, {1}, map square: {2}",
                                    deathEvent.Position.X, deathEvent.Position.Z, Utility.GetMapSquareFromPosition(deathEvent.Position));
                            } 
                            else 
                            {
                                Console.WriteLine("\tLost one crew member ({0}) at: {1}, {2}, map square: {3}",
                                    target.Name, deathEvent.Position.X, deathEvent.Position.Z, Utility.GetMapSquareFromPosition(deathEvent.Position));
                            }
                        }
                        break;

                    case MissionEventType.VehicleDestroyed:
                        {
                            DeathEvent deathEvent = evt as DeathEvent;
                            MissionObject attacker = deathEvent.GetAttackerObject();

                         
                            Console.ForegroundColor = ConsoleColor.Red;
                            if (attacker == null)
                                Console.WriteLine("\tAircraft crashed down.");
                            else
                                Console.WriteLine("\tAircraft was destroyed by fire of {0}.", attacker.Name);
                            
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine("\tReported take down place: ({0}; {1}), map square: {2}",
                                deathEvent.Position.X, deathEvent.Position.Z, Utility.GetMapSquareFromPosition(deathEvent.Position));
                        }
                        break;

                    case MissionEventType.Suicide:
                        {
                            SuicideEvent deathEvent = evt as SuicideEvent;
                            Console.ForegroundColor = ConsoleColor.Red;
                        
                            Console.WriteLine("\tCommited suicide.");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine("\tReported death place: {0}, {1}, map square: {2}",
                                    deathEvent.Position.X, deathEvent.Position.Z, Utility.GetMapSquareFromPosition(deathEvent.Position));
                        }
                        break;

                    case MissionEventType.MissionObjective:
                        {
                            MissionObjectiveEvent objective = evt as MissionObjectiveEvent;

                            if (objective.Coalition != Utility.GetCountryCoalition(countries, player.Country))
                                break;

                            if (objective.Completed)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write("\tSuccessfully completed");
                           
                            } 
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("\tFailed to complete");
                            }

                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine(" {0} objective, briefing order N{1}", 
                                objective.ObjectiveType, objective.MissionObjectiveId);
                        }
                        break;

                    case MissionEventType.MissionEnd:
                        if (gameSession.FinishState == SessionFinishState.Landed)
                            Console.WriteLine("\nAfter landing and securing aircraft safety");
                        Console.WriteLine("\nArrived to commander for reporting mission results.");
                        break;
                }
            }

            Console.WriteLine("\n----------------------------------------------------------\nRemarks by chief officer:");
            Console.ForegroundColor = ConsoleColor.White;


            if ((gameSession.FinishState == SessionFinishState.Landed) ||
                (gameSession.FinishState == SessionFinishState.LeftMission) ||
                (gameSession.FinishState == SessionFinishState.None))
            {
                Console.Write("\tArrived to base ");

                if (gameSession.PilotDamage > 0.8f)
                    Console.WriteLine("almost dead.");
                else if (gameSession.PilotDamage > 0.5)
                    Console.WriteLine("heavily wounded.");
                else
                    Console.WriteLine("without significant injures.");

                Console.Write("\tAircraft state: ");


                if (gameSession.VehicleDamage > 0.8f)
                    Console.WriteLine("unrecoverable.");
                else if (gameSession.VehicleDamage > 0.3)
                    Console.WriteLine("needs maintenance.");
                else
                    Console.WriteLine("ready for next mission.");
            }

            Console.WriteLine("\tConfirmed destroyed objects:");

            PlayerStatsInfo info = player.GetStats(missionLog.MissionCountries);
            UnitKillCount[] kills = info.KilledUnitsCount;
            
            int count = 0;

            for (int i = 0; i < kills.Length; ++i)
            {
                if (kills[i].Count == 0)
                    continue;

                switch (kills[i].UnitType)
                {
                    case UnitType.Static:
                        continue;

                    case UnitType.Unknown:
                        continue;
                }

                count += kills[i].Count;

                Console.WriteLine("\t\t{0} : {1}", kills[i].UnitType, kills[i].Count);
            }

            if (count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\tNo any objects reported by intelligence, civils and observers.");
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (count > 20)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\tRecommended for moving in rank.");
                Console.ForegroundColor = ConsoleColor.White;
            }
            else
            {
                Console.WriteLine("\t{0} targets at all.", count);
            }
            

        }

		public static void Main (string[] args)
		{
            Deathnote.GroundFilterMaxSeconds = 30;
            Deathnote.MinValidDamage = 0.35f;

            string filename = (args.Length != 0) ? args[0] : null;

#if DEBUG
			//string filename = @"/Users/taleks/Downloads/data/missionReport(2015-03-08_03-33-51)[0].txt";
#endif
            if (filename == null)
                return;

			LogFile logFile = LogFile.Parse(filename, true);

			if (!logFile.HaveLogEntries())
				return;

            GameObjectsData data = LoadGameData();
			MissionLog missionLog = new MissionLog(logFile, data);

			string missionDumpFile = string.Format("{0}.json", logFile.LogId);
			string missionLogFile = string.Format("{0}-log.json", logFile.LogId);

            ExportOptions opts = new ExportOptions();
            opts.ExportAllPlayers = true;
            opts.ExportInteresting = true;
            var exportData = missionLog.GetExportData(opts);

            SerializeToJson(exportData, missionDumpFile);

			//SerializeToJson(missionLog, missionDumpFile);
			SerializeToJson(logFile, missionLogFile);

            /*BriefStat brief = new BriefStat();
            brief.DeadStats = missionLog.DeadStats;
            brief.MissionStats = missionLog.MissionStats;
            brief.PlayerStats = missionLog.GetAllPlayerStats();
            string briefDumpFile = string.Format("{0}-brief.json", logFile.LogId);

            SerializeToJson(brief, briefDumpFile);
            */
            if (Utility.IsSingleMissionType(missionLog.MissionType))
            {
                DumpPlayer(missionLog, logFile.LogId);
            }
            else
            {
                DumpCountries(missionLog);
                DumpPlayers(missionLog);
            }
		}
	}
}
