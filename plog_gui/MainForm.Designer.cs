﻿namespace plog_gui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.startButton = new System.Windows.Forms.Button();
            this.filePath = new System.Windows.Forms.TextBox();
            this.selectPathButton = new System.Windows.Forms.Button();
            this.conversionProgress = new System.Windows.Forms.ProgressBar();
            this.selectFilepathDialog = new System.Windows.Forms.OpenFileDialog();
            this.selectLogsFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // startButton
            // 
            resources.ApplyResources(this.startButton, "startButton");
            this.startButton.Name = "startButton";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // filePath
            // 
            resources.ApplyResources(this.filePath, "filePath");
            this.filePath.Name = "filePath";
            // 
            // selectPathButton
            // 
            resources.ApplyResources(this.selectPathButton, "selectPathButton");
            this.selectPathButton.Name = "selectPathButton";
            this.selectPathButton.UseVisualStyleBackColor = true;
            this.selectPathButton.Click += new System.EventHandler(this.selectPathButton_Click);
            // 
            // conversionProgress
            // 
            resources.ApplyResources(this.conversionProgress, "conversionProgress");
            this.conversionProgress.Name = "conversionProgress";
            // 
            // selectFilepathDialog
            // 
            this.selectFilepathDialog.FileName = "openFileDialog1";
            resources.ApplyResources(this.selectFilepathDialog, "selectFilepathDialog");
            // 
            // selectLogsFolder
            // 
            resources.ApplyResources(this.selectLogsFolder, "selectLogsFolder");
            this.selectLogsFolder.ShowNewFolderButton = false;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.conversionProgress);
            this.Controls.Add(this.selectPathButton);
            this.Controls.Add(this.filePath);
            this.Controls.Add(this.startButton);
            this.Name = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TextBox filePath;
        private System.Windows.Forms.Button selectPathButton;
        private System.Windows.Forms.ProgressBar conversionProgress;
        private System.Windows.Forms.OpenFileDialog selectFilepathDialog;
        private System.Windows.Forms.FolderBrowserDialog selectLogsFolder;
        private System.Windows.Forms.Button button1;
    }
}

