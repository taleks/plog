﻿namespace plog_gui
{
    partial class MissionStatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MissionStatsForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statsGrid = new System.Windows.Forms.DataGridView();
            this.PlayerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kills = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GunnerKills = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlaneKills = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Flights = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Crashes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Landings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ejects = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeftMission = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deaths = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VehiclesLost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fighters = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Attackers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bombers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AAA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirePoints = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artillery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tanks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vehicles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Turrets = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Statics = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // statsGrid
            // 
            this.statsGrid.AllowUserToAddRows = false;
            this.statsGrid.AllowUserToDeleteRows = false;
            this.statsGrid.AllowUserToOrderColumns = true;
            resources.ApplyResources(this.statsGrid, "statsGrid");
            this.statsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.statsGrid.BackgroundColor = System.Drawing.Color.Black;
            this.statsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlayerName,
            this.Country,
            this.Kills,
            this.GunnerKills,
            this.PlaneKills,
            this.Flights,
            this.Crashes,
            this.Landings,
            this.Ejects,
            this.LeftMission,
            this.Deaths,
            this.VehiclesLost,
            this.Fighters,
            this.Attackers,
            this.Bombers,
            this.AAA,
            this.FirePoints,
            this.Artillery,
            this.Tanks,
            this.Vehicles,
            this.Turrets,
            this.Statics});
            this.statsGrid.MultiSelect = false;
            this.statsGrid.Name = "statsGrid";
            this.statsGrid.ReadOnly = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.statsGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.statsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.statsGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.statsGrid_CellFormatting);
            // 
            // PlayerName
            // 
            this.PlayerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PlayerName.DataPropertyName = "PlayerName";
            resources.ApplyResources(this.PlayerName, "PlayerName");
            this.PlayerName.Name = "PlayerName";
            this.PlayerName.ReadOnly = true;
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            resources.ApplyResources(this.Country, "Country");
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            // 
            // Kills
            // 
            this.Kills.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Kills.DataPropertyName = "Kills";
            resources.ApplyResources(this.Kills, "Kills");
            this.Kills.Name = "Kills";
            this.Kills.ReadOnly = true;
            // 
            // GunnerKills
            // 
            this.GunnerKills.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GunnerKills.DataPropertyName = "GunnerKills";
            resources.ApplyResources(this.GunnerKills, "GunnerKills");
            this.GunnerKills.Name = "GunnerKills";
            this.GunnerKills.ReadOnly = true;
            // 
            // PlaneKills
            // 
            this.PlaneKills.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PlaneKills.DataPropertyName = "PlaneKills";
            resources.ApplyResources(this.PlaneKills, "PlaneKills");
            this.PlaneKills.Name = "PlaneKills";
            this.PlaneKills.ReadOnly = true;
            // 
            // Flights
            // 
            this.Flights.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Flights.DataPropertyName = "Flights";
            resources.ApplyResources(this.Flights, "Flights");
            this.Flights.Name = "Flights";
            this.Flights.ReadOnly = true;
            // 
            // Crashes
            // 
            this.Crashes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Crashes.DataPropertyName = "Crashes";
            resources.ApplyResources(this.Crashes, "Crashes");
            this.Crashes.Name = "Crashes";
            this.Crashes.ReadOnly = true;
            // 
            // Landings
            // 
            this.Landings.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Landings.DataPropertyName = "Landings";
            resources.ApplyResources(this.Landings, "Landings");
            this.Landings.Name = "Landings";
            this.Landings.ReadOnly = true;
            // 
            // Ejects
            // 
            this.Ejects.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Ejects.DataPropertyName = "Ejects";
            resources.ApplyResources(this.Ejects, "Ejects");
            this.Ejects.Name = "Ejects";
            this.Ejects.ReadOnly = true;
            // 
            // LeftMission
            // 
            this.LeftMission.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LeftMission.DataPropertyName = "LeftMission";
            resources.ApplyResources(this.LeftMission, "LeftMission");
            this.LeftMission.Name = "LeftMission";
            this.LeftMission.ReadOnly = true;
            // 
            // Deaths
            // 
            this.Deaths.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Deaths.DataPropertyName = "Deaths";
            resources.ApplyResources(this.Deaths, "Deaths");
            this.Deaths.Name = "Deaths";
            this.Deaths.ReadOnly = true;
            // 
            // VehiclesLost
            // 
            this.VehiclesLost.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VehiclesLost.DataPropertyName = "VehiclesLost";
            resources.ApplyResources(this.VehiclesLost, "VehiclesLost");
            this.VehiclesLost.Name = "VehiclesLost";
            this.VehiclesLost.ReadOnly = true;
            // 
            // Fighters
            // 
            this.Fighters.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Fighters.DataPropertyName = "Fighters";
            resources.ApplyResources(this.Fighters, "Fighters");
            this.Fighters.Name = "Fighters";
            this.Fighters.ReadOnly = true;
            // 
            // Attackers
            // 
            this.Attackers.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Attackers.DataPropertyName = "Attackers";
            resources.ApplyResources(this.Attackers, "Attackers");
            this.Attackers.Name = "Attackers";
            this.Attackers.ReadOnly = true;
            // 
            // Bombers
            // 
            this.Bombers.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Bombers.DataPropertyName = "Bombers";
            resources.ApplyResources(this.Bombers, "Bombers");
            this.Bombers.Name = "Bombers";
            this.Bombers.ReadOnly = true;
            // 
            // AAA
            // 
            this.AAA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AAA.DataPropertyName = "AAA";
            resources.ApplyResources(this.AAA, "AAA");
            this.AAA.Name = "AAA";
            this.AAA.ReadOnly = true;
            // 
            // FirePoints
            // 
            this.FirePoints.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FirePoints.DataPropertyName = "FirePoints";
            resources.ApplyResources(this.FirePoints, "FirePoints");
            this.FirePoints.Name = "FirePoints";
            this.FirePoints.ReadOnly = true;
            // 
            // Artillery
            // 
            this.Artillery.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Artillery.DataPropertyName = "Artillery";
            resources.ApplyResources(this.Artillery, "Artillery");
            this.Artillery.Name = "Artillery";
            this.Artillery.ReadOnly = true;
            // 
            // Tanks
            // 
            this.Tanks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Tanks.DataPropertyName = "Tanks";
            resources.ApplyResources(this.Tanks, "Tanks");
            this.Tanks.Name = "Tanks";
            this.Tanks.ReadOnly = true;
            // 
            // Vehicles
            // 
            this.Vehicles.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Vehicles.DataPropertyName = "Vehicles";
            resources.ApplyResources(this.Vehicles, "Vehicles");
            this.Vehicles.Name = "Vehicles";
            this.Vehicles.ReadOnly = true;
            // 
            // Turrets
            // 
            this.Turrets.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Turrets.DataPropertyName = "Turrets";
            resources.ApplyResources(this.Turrets, "Turrets");
            this.Turrets.Name = "Turrets";
            this.Turrets.ReadOnly = true;
            // 
            // Statics
            // 
            this.Statics.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Statics.DataPropertyName = "Statics";
            resources.ApplyResources(this.Statics, "Statics");
            this.Statics.Name = "Statics";
            this.Statics.ReadOnly = true;
            // 
            // MissionStatsForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statsGrid);
            this.Name = "MissionStatsForm";
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView statsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlayerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kills;
        private System.Windows.Forms.DataGridViewTextBoxColumn GunnerKills;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlaneKills;
        private System.Windows.Forms.DataGridViewTextBoxColumn Flights;
        private System.Windows.Forms.DataGridViewTextBoxColumn Crashes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Landings;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ejects;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeftMission;
        private System.Windows.Forms.DataGridViewTextBoxColumn Deaths;
        private System.Windows.Forms.DataGridViewTextBoxColumn VehiclesLost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fighters;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attackers;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bombers;
        private System.Windows.Forms.DataGridViewTextBoxColumn AAA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirePoints;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artillery;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tanks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vehicles;
        private System.Windows.Forms.DataGridViewTextBoxColumn Turrets;
        private System.Windows.Forms.DataGridViewTextBoxColumn Statics;
        //private System.Windows.Forms.DataGridViewTextBoxColumn PlayerName;
    }
}