﻿using ploggy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace plog_gui
{
    struct BriefStat
    {
        public MissionStats MissionStats;
        public List<DeathSourceClassifier> DeadStats;
        public List<PlayerStatsInfo> PlayerStats;
    }

    public class BriefPlayerStats
    {
        private string playerName;
        public string PlayerName { get { return playerName; } }
        
        private Country country;
        private string countryString = null;
        public int kills;
        private int gunnerKills;
        private int planeKills;
        private int flights;
        private int crashes;
        private int landings;
        private int ejects;
        private int leftMission;

        private int deaths;
        private int vehiclesLost;

        private int fighters;
        private int attackers;
        private int bombers;
        private int aAA;
        private int firePoints;
        private int artillery;
        private int tanks;
        private int vehicles;
        private int turrets;
        private int statics;

        private bool changedCountries = false;

        public string Country { get { return countryString; } }
        public string Kills { get { return kills.ToString(); } }
        public string GunnerKills { get { return gunnerKills.ToString(); } }
        public string PlaneKills { get { return planeKills.ToString(); } }
        public string Flights { get { return flights.ToString(); } }
        public string Crashes { get { return crashes.ToString(); } }
        public string Landings { get { return landings.ToString(); } }
        public string Ejects { get { return ejects.ToString(); } }
        public string LeftMission { get { return leftMission.ToString(); } }

        public string Deaths { get { return deaths.ToString(); } }
        public string VehiclesLost { get { return vehiclesLost.ToString(); } }

        public string Fighters { get { return fighters.ToString(); } }
        public string Attackers { get { return attackers.ToString(); } }
        public string Bombers { get { return bombers.ToString(); } }
        public string AAA { get { return aAA.ToString(); } }
        public string FirePoints { get { return firePoints.ToString(); } }
        public string Artillery { get { return artillery.ToString(); } }
        public string Tanks { get { return tanks.ToString(); } }
        public string Vehicles { get { return vehicles.ToString(); } }
        public string Turrets { get { return turrets.ToString(); } }
        public string Statics { get { return statics.ToString(); } }

        public BriefPlayerStats(PlayerStatsInfo psi)
        {
            playerName = psi.Name;
            
            country = psi.PriorityCountry;
            changedCountries = psi.GetPlayer().ChangedCountry;

            countryString = string.Format("{0}{1}", country, changedCountries ? "*" : "");

            kills = psi.Kills;
            gunnerKills = psi.GunnerKills;
            planeKills = psi.VehicleKills;

            flights = psi.Flights;
            crashes = psi.Crashes;
            landings = psi.Landings;
            ejects = psi.Ejects;
            leftMission = psi.LeftMissions;

            deaths = psi.Deaths;
            vehiclesLost = psi.VehiclesLost;

            UnitKillCount[] ukc = psi.KilledUnitsCount;
            fighters = ukc[(int)UnitType.Fighter].Count;
            attackers = ukc[(int)UnitType.Attacker].Count;
            bombers = ukc[(int)UnitType.Bomber].Count;
            aAA = ukc[(int)UnitType.AAA].Count;
            firePoints = ukc[(int)UnitType.FiringPoint].Count;
            artillery = ukc[(int)UnitType.Artillery].Count;
            tanks = ukc[(int)UnitType.Tank].Count;
            vehicles = ukc[(int)UnitType.Vehicle].Count;
            turrets = ukc[(int)UnitType.Turret].Count;
            statics = ukc[(int)UnitType.Static].Count;


        }

        public Country GetCountry()
        {
            return country;
        }
    }
}
