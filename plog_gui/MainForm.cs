﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using ploggy;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text.RegularExpressions;

namespace plog_gui
{
    public partial class MainForm : Form
    {
        private MissionStatsForm missionStatsForm = new MissionStatsForm();

        private GameObjectsData LoadGameObjectsData()
        {
            Dictionary<string, UnitType> gameData = null;

            try
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new StringEnumConverter());

                using (StreamReader sw = new StreamReader("GameObjects.json"))
                using (JsonReader reader = new JsonTextReader(sw))
                {
                    gameData = serializer.Deserialize<Dictionary<string, UnitType>>(reader);
                }

                return new GameObjectsData(gameData);
            }

            catch (Exception e)
            {
            }

            return null;
        }

        public static GameObjectsData GameObjectsData = null;

        public MainForm()
        {
            InitializeComponent();
            GameObjectsData = LoadGameObjectsData();
        }

        private void selectPathButton_Click(object sender, EventArgs e)
        {
            if (selectFilepathDialog.ShowDialog() != DialogResult.OK)
                return;

            filePath.Text = selectFilepathDialog.FileName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (selectLogsFolder.ShowDialog() != DialogResult.OK)
                return;

            filePath.Text = selectLogsFolder.SelectedPath;
        }

        private void EnableStartButton()
        {
            BeginInvoke((Action)(() =>
            {
                startButton.Enabled = true;
            }));
        }

        private void UpdateProgress(int current, int maximum)
        {
            BeginInvoke((Action)(() =>
            {
                conversionProgress.Maximum = maximum;
                conversionProgress.Value = current;
            }));
        }

        private void EnumerateFilesInFolder(string folderName, List<string> fileList)
        {
            string[] files = Directory.GetFiles(folderName);
            Regex pathRegex = new Regex(@"(.+)\((.+)\)\[(\d+)\](.+)");
            Dictionary<string, bool> idMap = new Dictionary<string, bool>(32);

            for (int i = 0; i < files.Length; ++i)
            {
                string pathToFile = files[i];

                Match match = pathRegex.Match(pathToFile);

                if (!match.Success)
                    continue;
                
                string idString = match.Groups[2].Value;

                if (idMap.ContainsKey(idString))
                    continue;

                idMap.Add(idString, true);
                fileList.Add(pathToFile);
            }
        }

        private static void SerializeToJson(Object obj, string filename)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new StringEnumConverter());
            serializer.NullValueHandling = NullValueHandling.Include;
            serializer.Formatting = Formatting.Indented;

            using (StreamWriter sw = new StreamWriter(filename))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, obj);
            }
        }

        private void plog_Worker(object pathObj)
        {
#if !DEBUG
            try
            {
#endif
                string path = pathObj as string;

                if ((GameObjectsData == null) || (path == null))
                {
                    EnableStartButton();
                    return;
                }

                List<string> fileList = new List<string>(16);

                if (Directory.Exists(path))
                    EnumerateFilesInFolder(path, fileList);
                else if (File.Exists(path))
                    fileList.Add(path);

                int count = fileList.Count;

                if (count == 0)
                    return;

                MissionLog missionLog = null;
                UpdateProgress(0, count);

                for (int i = 0; i < count; ++i)
                {
                    string dir = Path.GetDirectoryName(fileList[i]);
                    LogFile logFile = LogFile.Parse(fileList[i], true);

                    if (logFile.HaveLogEntries())
                    {
                        missionLog = new MissionLog(logFile, GameObjectsData);

                        string missionDumpFile = string.Format("{0}.json", logFile.LogId);
                        string missionLogFile = string.Format("{0}-log.json", logFile.LogId);
                        string briefDumpFile = string.Format("{0}-brief.json", logFile.LogId);

                        string pathToSave = Path.Combine(dir, missionDumpFile);
                        SerializeToJson(missionLog, pathToSave);

                        pathToSave = Path.Combine(dir, missionLogFile);
                        SerializeToJson(logFile, pathToSave);

                        BriefStat brief = new BriefStat();
                        brief.DeadStats = missionLog.DeadStats;
                        brief.MissionStats = missionLog.MissionStats;
                        brief.PlayerStats = missionLog.GetAllPlayerStats();

                        pathToSave = Path.Combine(dir, briefDumpFile);
                        SerializeToJson(brief, pathToSave);
                    }

                    UpdateProgress(i, count);
                }

                UpdateProgress(count, count);
                EnableStartButton();

                if (missionLog == null)
                    return;

                ShowResults(missionLog);
#if !DEBUG
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
#endif
        }



        private void ShowResults(MissionLog missionLog)
        {
            missionStatsForm.SetData(missionLog);

            BeginInvoke((Action)(() =>
            {
                missionStatsForm.ShowDialog();
            }));
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            ThreadPool.QueueUserWorkItem(plog_Worker, filePath.Text);
        }
    }
}
